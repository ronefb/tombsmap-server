const express = require('express');
require('./src/database')
require('./src/config')
require("dotenv-safe").config({path: __dirname + '/.env'});

var cors = require('cors')
const app =  express();

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())


app.use(cors());

const path = require('path');
app.use('/public', express.static(path.join(__dirname, 'public')));

//Require Controller
const ctrUsuario =  require("./src/controller/ControllerUsuario")
const ctrPermissao =  require("./src/controller/ControllerPermissao")
const ctrCemiterio =  require("./src/controller/ControllerCemiterio")
const ctrResponsavel = require("./src/controller/ControllerResponsavel");

//User Controller
app.use('/api/usuario', ctrUsuario);
app.use('/api/permissao', ctrPermissao);
app.use('/api/cemiterio', ctrCemiterio);
app.use('/api/responsavel', ctrResponsavel);



app.listen(process.env.PORT || 3000);