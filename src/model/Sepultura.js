const { Model, DataTypes } = require('sequelize');


class Sepultura extends Model{
    static init(sequelize){
        super.init({    
            numero: DataTypes.INTEGER,
            foto: DataTypes.STRING,
            setor: DataTypes.STRING,
            referencia: DataTypes.STRING,
            cemiterio_id: DataTypes.INTEGER,
            geo_id: DataTypes.INTEGER
        }, {
            sequelize,
            tableName: 'sepultura'
        })
    }

    static associate(models){
        this.hasOne(models.Geolocalizacao, { foreignKey:'id', sourceKey: 'geo_id', as: 'geolocalizacao' });
        this.hasOne(models.Cemiterio, { foreignKey:'cemiterio_id', as: 'cemiterio' });
        this.belongsToMany(models.Falecido, { foreignKey:'sepultura_id', through: 'sepultura_falecido', as: 'falecido'})
    }
}

module.exports = Sepultura;