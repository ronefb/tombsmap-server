const { Model, DataTypes, QueryTypes } = require('sequelize');


class Cemiterio extends Model{
    static init(sequelize){
        super.init({    
            nome: DataTypes.STRING,
            cep: DataTypes.STRING,
            cidade: DataTypes.STRING,
            estado: DataTypes.STRING,
            pais: DataTypes.STRING,
            bairro: DataTypes.STRING,
            rua: DataTypes.STRING,
            numero: DataTypes.INTEGER,
	        geo_id: DataTypes.INTEGER
        }, {
            sequelize,
            tableName: 'cemiterio'
        })
    }
   
    static associate(models){
        this.hasOne(models.Geolocalizacao, { foreignKey:'id', sourceKey: 'geo_id', as: 'geo' });
        this.belongsToMany(models.Responsavel, { foreignKey:'cemiterio_id', through: 'cemiterio_responsavel', as: 'responsavel'})  
    }
}

module.exports = Cemiterio;