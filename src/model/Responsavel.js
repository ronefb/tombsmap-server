const { Model, DataTypes } = require('sequelize');

class Responsavel extends Model{
    static init(sequelize){
        super.init({    
            nome_organizacao: DataTypes.STRING
        }, {
            sequelize,
            tableName: 'responsavel'
        })
    }

  

    static associate(models){
        this.belongsTo(models.Usuario, { foreignKey:'usu_id', as: 'usuarios' })
        this.belongsToMany(models.Cemiterio, { foreignKey:'responsavel_id', through: 'cemiterio_responsavel', as: 'cemiterios'})
    }
}

module.exports = Responsavel;