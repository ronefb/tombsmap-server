const { Model, DataTypes } = require('sequelize');


class Geolocalizacao extends Model{
    static init(sequelize){
        super.init({    
            latitude: DataTypes.DECIMAL,
            longitude: DataTypes.DECIMAL,
            place_id: DataTypes.INTEGER
        }, {
            sequelize,
            tableName: 'geolocalizacao',
        })
    }

}

module.exports = Geolocalizacao;