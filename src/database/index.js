const Sequelize = require('sequelize');
/* Conection init */
const dbConfig = require('../config/database');
const connection  = new Sequelize(dbConfig);
/* Models Init */
const Usuario = require('../model/Usuario');
const Permissao = require('../model/Permissao');
const Cemiterio = require('../model/Cemiterio');
const Responsavel = require('../model/Responsavel');
const Geolocalizacao = require('../model/Geolocalizacao');
const Tomb = require('../model/Sepultura');
const Falecido = require('../model/Falecido');


Usuario.init(connection)
Permissao.init(connection)
Cemiterio.init(connection)
Responsavel.init(connection)
Geolocalizacao.init(connection)
Tomb.init(connection)
Falecido.init(connection)


Usuario.associate(connection.models)
Permissao.associate(connection.models)
Responsavel.associate(connection.models)
Cemiterio.associate(connection.models)
Tomb.associate(connection.models)
Falecido.associate(connection.models)

module.exports = connection;