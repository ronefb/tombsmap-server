'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('usuario_permissoes', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      usu_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'usuario', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      permissoes_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'permissoes', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      status:{
        type: Sequelize.INTEGER,
  
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,

      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_rules');
  }
};
