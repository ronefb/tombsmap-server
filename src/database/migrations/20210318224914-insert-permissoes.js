'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.bulkInsert('permissoes', [{
      nome:"USU",
      descricao:"Usuario Comum",
      created_at: new Date(),
      updated_at: new Date()
    }]);
    queryInterface.bulkInsert('permissoes', [{
      nome:"EMP",
      descricao:"Empresas",
      created_at: new Date(),
      updated_at: new Date()
    }]);
    queryInterface.bulkInsert('permissoes', [{
      nome:"ADM",
      descricao:"Administrador",
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
