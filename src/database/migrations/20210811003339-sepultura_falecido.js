'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sepultura_falecido', {  
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      sepultura_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'sepultura', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      falecido_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'falecido', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('sepultura_falecido');
  }
};
