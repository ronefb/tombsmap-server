'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('sepultura', { 
       id: {
         type: Sequelize.INTEGER,
         primaryKey: true,
         autoIncrement: true,
         allowNull: false
       },
       numero: {
        type: Sequelize.INTEGER,
        allowNull: false
       },
       foto: {
        type: Sequelize.STRING,
       },
       referencia: {
        type: Sequelize.STRING,
       },
       setor: {
        type: Sequelize.STRING,
       },
       cemiterio_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'cemiterio', key: 'id'}
       },
       geo_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'geolocalizacao', key: 'id'}
       },
       created_at: {
         type: Sequelize.DATE,
         allowNull: false,
       },
       updated_at: {
         type: Sequelize.DATE,
         allowNull: false,
       }
     });
    
 },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tombs');
  }
};
