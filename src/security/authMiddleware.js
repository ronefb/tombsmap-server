const jwt = require('jsonwebtoken');
const config = require("../config");

module.exports = (credentials = []) => {
    
    return (req, res, next) => {
        
      if (typeof credentials === "string") {
        credentials = [credentials];
      }
      const token = req.headers["authorization"];
      if (!token) {
        return res.status(401).send("Acesso negado.");
      } else {
        const tokenBody = token.slice(7);
        jwt.verify(tokenBody, process.env.SECRET, (err, decoded) => {
          if (err) return res.status(401).send("Acesso negado.");
          if (credentials.length > 0) {
            if (
              decoded.scopes &&
              decoded.scopes.length &&
              credentials.some(cred => decoded.scopes.indexOf(cred) >= 0)
            ) {
              next();
            } else {
              return res.status(401).send("Acesso negado.");
            }
          } else {
            next();
          }
        });
      }
    };
  };