const express =  require("express");
const router = express.Router();
const authorize = require("../security/authMiddleware");
const CemiterioService = require('../service/CemiterioService');
const FalecidoService = require("../service/FalecidoService");
const SepulturaService = require('../service/SepulturaService');
/* base route /api/cemiterio */
router.get('/', CemiterioService.index);
router.get('/:id', CemiterioService.show);
router.get('/responsavel/:responsavel_id',authorize(), CemiterioService.showResponsavel);
router.post('/:usu_id',authorize(), CemiterioService.store);
router.put('/:cemiterio_id',authorize(), CemiterioService.update);
router.delete('/:cemiterio_id',authorize(), CemiterioService.destroy);


router.get('/sepulturas/:cemiterio_id',authorize(), SepulturaService.index);
router.get('/sepulturas/:cemiterio_id/:nome',authorize(), SepulturaService.show);
router.post('/sepulturas/:cemiterio_id',authorize(), SepulturaService.store);
router.put('/sepulturas/:cemiterio_id/:sepultura_id',authorize(), SepulturaService.update);
router.delete('/sepulturas/:cemiterio_id/:sepultura_id',authorize(), SepulturaService.destroy);

router.post('/sepultura/foto/:cemiterio_id/:sepultura_id',authorize(), SepulturaService.foto);

router.post('/falecido/:cemiterio_id', authorize(), FalecidoService.store);
router.get('/falecido/:cemiterio_id', authorize(), FalecidoService.index);
router.get('/falecido/:cemiterio_id/:nome',authorize(), FalecidoService.show);
router.post('falecido/upload', authorize(), FalecidoService.upload)
module.exports = router;

