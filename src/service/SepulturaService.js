
const Sepultura = require('../model/Sepultura');
const Geolocalizacao = require('../model/Geolocalizacao');
const Falecido = require('../model/Falecido');
const { sequelize } = Geolocalizacao;
const { Op } = require('sequelize');
module.exports = {

    async index(req, res){
        var query = {
            include: [
                {
                    model: Falecido, as: 'falecido', 
                    attributes: {
                        exclude:['updatedAt', 'createdAt', 'local_sepultamento']
                    }
                },
                {
                    model: Geolocalizacao, as: 'geolocalizacao', 
                    attributes: {
                        exclude:['geo_id', 'updatedAt', 'createdAt', 'id', 'place_id']
                    }
                }
            ],
            attributes: {
                exclude:['updatedAt', 'createdAt']
            }
        };
        const sepulturas = await Sepultura.findAll(query);
        return res.json(sepulturas);
    },
    async store(req, res){
       
        const { geo, falecido } = req.body;
       
        if(!falecido) res.status(400).send(`É obrigatório ter um falecido vinculado ao túmulo! `);
        if(!geo) res.status(400).send(`É obrigatório ter uma localização! `);

        const findFalecido = await Falecido.findByPk(falecido.id);
        
        if(findFalecido == null)
            res.status(400).send(`Não foi possivel localizar este falecido: `);

        return sequelize.transaction().then(function (t) {
            return Geolocalizacao.create(geo, {transaction: t}).then((geo) => {
                var geo_id = geo.getDataValue('id');
                const obj = {...req.body, 'cemiterio_id': parseInt(req.params.cemiterio_id), geo_id};
                return Sepultura.create(obj, {transaction: t}).then((sepultura) => {
                    findFalecido.set({id_sepultura: sepultura.id});
                    return sepultura.addFalecido(findFalecido, {transaction: t}).then(() => {
                         res.status(200).json(sepultura);
                         return t.commit();
                    });
                });
            }).catch((err) => {
                t.rollback();
                if(err.errors && err.errors.length > 0) {
                    const { type } = err.errors[0];
                    if(type === "unique violation") return res.status(400).send(`Já existe cadastro na sepultura: ` + req.body.numero)
                }
                return res.status(400).send(`Erro ao cadastrar sepultura`)
            });
        });
    },
    async foto(req, res){
        const formidable = require('formidable');
        const fs = require('fs');
        const form = new formidable.IncomingForm();
        form.parse(req, (err, fields, files) => {
            const { cemiterio_id, sepultura_id } = req.params;

            const fileName = `foto_cemi_${cemiterio_id}_sepul_${sepultura_id}`;

            var imgBase64  =  fields.image.replace(/^data:image\/png;base64,/, "");
            imgBase64  +=  imgBase64.replace('+', ' ');
            var imageBuffer = Buffer.from(imgBase64, 'base64').toString('binary');
       
            fs.writeFile(`public/images/${fileName}.png`, imageBuffer, "binary", function () {
                Sepultura.update({foto: `public/images/${fileName}.png`}, {where: {id: sepultura_id, cemiterio_id: cemiterio_id}});
                return res.send('Foto Salva');
            }).catch((err) => {
				return res.status(400).send(`Erro ao salvar foto: ${err}`)
			});
           
    

        });
        
    },
    async show (req, res){
        var query = {
            include: [
                {
                    model: Falecido, as: 'falecido', 
                    attributes: {
                        exclude:['updatedAt', 'createdAt', 'local_sepultamento']
                    },
                    where: {
                        nome: { [Op.like]: `%${req.params.nome}%` }
                    }
                },
                {
                    model: Geolocalizacao, as: 'geolocalizacao', 
                    attributes: {
                        exclude:['geo_id', 'updatedAt', 'createdAt', 'id', 'place_id']
                    }
                }
            ],
            attributes: {
                exclude:['updatedAt', 'createdAt']
            }
        }
        const sepulturas = await Sepultura.findAll(query)
        return res.json(sepulturas)
    },
    async update(req, res){
        const sepultura = await Sepultura.update(req.body, {where : {id: req.params.sepultura_id}});
        const find = await Sepultura.findOne({where : {id: sepultura}});
        return res.json(find);
    },
    async destroy(req, res){
        await Sepultura.destroy({where: {id: req.params.sepultura_id}});
        return res.send();
    }
}