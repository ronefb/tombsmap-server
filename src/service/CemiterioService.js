const Cemiterio = require('../model/Cemiterio');
const Responsavel = require('../model/Responsavel');
const Geolocalizacao = require('../model/Geolocalizacao');
const { sequelize } = Geolocalizacao;

module.exports = {

    async index(req, res){
        const cemiterio = await Cemiterio.findAll(
            {
                include: [
                    {
                        association: 'geo',
                        attributes: {
                            exclude:['id', 'createdAt', 'updatedAt']
                        }
                    }
                ],
                attributes: {exclude: ['cemiterio_id', 'geo_id', 'createdAt', 'updatedAt']
            }
        });
        return res.status(200).json(cemiterio);
    },

    async store(req, res){
        const responsavel = await Responsavel.findOne({where: {usu_id: req.params.usu_id}});
        if(!responsavel) {
	        res.send("Usuário não encontrado");
        	return res.status(400)
        }
        return sequelize.transaction().then(function (t) {
        	var geo = {...req.body.geo, place_id: req.body.place_id};
			return Geolocalizacao.create(geo, {transaction: t}).then((geo) => {
				var geo_id = geo.getDataValue('id');
				var parms = {...req.body, geo_id:geo_id};
				return Cemiterio.create( parms,{transaction: t}).then((cemiterio) => {
					res.status(200).json(cemiterio);
                    cemiterio.addResponsavel(responsavel)
					return t.commit();
				})
			}).catch((err) => {
				res.status(402).send(`Erro ao cadastrar cemiterio: ${err}`)
				return t.rollback();
			})
        })
    },
    async show (req, res){    
        var query = {
            where: {
                id: req.parms.id
            },
            attributes: {
                exclude:['cemiterio_id']
            }
        };
        const cemiterio = await Cemiterio.findOne(query);
        return res.json(cemiterio)
    },
    async showResponsavel (req, res){    
        var query = {
            include: [
                {
                    association: 'geo',
                    attributes: {
                        exclude:['geo_id']
                    }
                },
                {
                    association: 'responsavel',
                    include: {
                        association: 'usuarios'
                    },
                    where: {
                        usu_id: req.params.responsavel_id
                    }
                }
            ],
            attributes: {
                exclude:['cemiterio_id']
            }
        };
        const cemiterio = await Cemiterio.findAll(query);
        return res.json(cemiterio)
    },
    async update(req, res){
        const cemiterio = await Cemiterio.update(req.body, {where : {id: req.params.cemiterio_id}});
        const find = await Cemiterio.findOne({where : {id: cemiterio}});
        return res.json(find);
    },
    async destroy(req, res){
        await Cemiterio.destroy({where: {id: req.params.cemiterio_id}});
        return res.send();
    }
}