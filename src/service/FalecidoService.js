const Cemiterio = require('../model/Cemiterio');
const Falecido = require('../model/Falecido');
const Sepultura = require('../model/Sepultura');
const Geolocalizacao = require('../model/Geolocalizacao');
const moment = require('moment');
moment().format(); 

const { Op } = require('sequelize');

module.exports = {

    async index(req, res){
        var query = {
            include: [
                {
                    model: Sepultura, as: 'sepultura',
                    attributes: {
                        exclude:['updatedAt', 'createdAt']
                    },
                    include:[
                        {
                            model: Geolocalizacao, as: 'geolocalizacao', 
                            attributes: {
                                exclude:['geo_id', 'updatedAt', 'createdAt', 'id', 'place_id']
                            }
                        }
                    ]
                }
              
            ],
            where: { cemiterio_id: req.params.cemiterio_id },
            attributes: {
                exclude:['updatedAt', 'createdAt']
            }
        }
        const falecidos =  await Falecido.findAll(query);
        return res.json(falecidos)

    },
    async store(req, res){
        
        const cemiterio = Cemiterio.findOne({where: {id: req.params.cemiterio_id}, attributes: {exclude: ['cemiterio_id']}});
        if(!cemiterio) {
        	return res.status(400).send("Cemitério não encontrado")
        }
      
        req.body.data_nascimento =  moment(req.body.data_nascimento, "DD-MM-YYYY");
        req.body.data_falecimento =  moment(req.body.data_falecimento, "DD-MM-YYYY");
        var nascimento = new Date(req.body.data_nascimento)
        var falecimento = new Date(req.body.data_falecimento)

        var idade = falecimento.getFullYear() - nascimento.getFullYear();
        if ( new Date(falecimento.getFullYear(), falecimento.getMonth(), falecimento.getDate()) < new Date(falecimento.getFullYear(), nascimento.getMonth(), nascimento.getDate()) )
            idade--;
        
        if(idade < 0 || !req.body.data_nascimento || !req.body.data_falecimento){
        	return res.status(400).send("As datas são inválidas!")
        }

        const cadastro = {...req.body, ...req.params, idade}
        const falecido = await Falecido.create(cadastro).catch((err) => {
            res.status(400).send(`Erro ao cadastrar falecido`)
        });
        return res.json(falecido)
    },
    async show (req, res){  
        
        var query = {
            include: [
                {
                    model: Sepultura, as: 'sepultura',
                    attributes: {
                        exclude:['updatedAt', 'createdAt']
                    },
                    include:[
                        {
                            model: Geolocalizacao, as: 'geolocalizacao', 
                            attributes: {
                                exclude:['geo_id', 'updatedAt', 'createdAt', 'id', 'place_id']
                            }
                        }
                    ]
                }
              
            ],
            where: {
                cemiterio_id: req.params.cemiterio_id,
                nome: { [Op.like]: `%${req.params.nome}%` }
            },
            attributes: {
                exclude:['updatedAt', 'createdAt']
            }
        }
        const falecidos =  await Falecido.findAll(query);
        return res.json(falecidos)
    },
    async upload(){
       
    },
    async update(req, res){
      
    },
    async destroy(req, res){
        
    }
}