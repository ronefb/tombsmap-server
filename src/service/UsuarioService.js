const { response } = require('express');
const Usuario = require('../model/Usuario');
const Responsavel = require('../model/Responsavel');
const Permissao = require('../model/Permissao');
const Cemiterio = require('../model/Cemiterio');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { sequelize } = require('../model/Usuario');
module.exports = {

    async index(req, res){
        const usuarios = await Usuario.findAll();
        return res.json(usuarios);
    },

    async store(req, res){
        const find  = await Usuario.findOne({where: {email: req.body.email}});
        if(find) return res.status(202).send("Usuário já cadastrado");
        
        req.body.senha = bcrypt.hashSync(req.body.senha, 10);
        
        const nome_permissao = req.body.nome_organizacao ? "EMP" : "USU";
        const permissao  = await Permissao.findOne({where: { nome: nome_permissao }})
        
        return sequelize.transaction().then(function (t) {
            return Usuario.create(req.body, {transaction: t}
            ).then(function (user) {
                usuario = user;
                return user.addPermissao(permissao, {status: 1, transaction: t});
            }).then(() =>{
                if(req.body.nome_organizacao){
                    return Responsavel.create({...req.body, usu_id:usuario.id}, {transaction: t}).then((responsavel) => {
                        res.status(200).json(usuario);
                        return t.commit();
                    });
                } else{
                    res.status(200).json(usuario);
                    return t.commit();
                }
            }).catch(function (err) {
                res.status(402).send(`Erro ao inserir usuário permissao: ${err}`)
                return t.rollback();
            });
        });
    },

    async login(req, res){
        const usuario  = await Usuario.findOne(
            {
                where: {
                    email: req.body.email
                },
                include: [
                    {
                        model: Permissao, as: 'permissao', 
                        attributes: ['nome']
                    }
                ]
            });
        if(!usuario || !bcrypt.compareSync(req.body.senha, usuario.senha))  return res.status(403).send("Usuário ou senha incorreto");
        
        const payload = {
            name: req.body.email,
            scopes: "customer:read"
        };
        const token = jwt.sign(payload, process.env.SECRET);

        delete usuario.senha;
        delete usuario.createdAt;
        delete usuario.updatedAt;

        var query = {
            include: [
                {
                    association: 'responsavel',
                    attributes: [] ,
                    include: {
                        association: 'usuarios',
                        attributes: []
                    },
                    where: {
                        id: usuario.id
                    }
                }
            ],
            attributes: {
                exclude:['cemiterio_id','updatedAt', 'createdAt']
            }
        };

        const cemiterios = await Cemiterio.findAll(query);
        res.header('Authorization', 'Bearer '+ token);
        return res.status(200).json({usuario, token, cemiterios});
            
        
    },
    async show (req, res){
        const usuario = await Usuario.findOne({where : {id: req.params.usuario_id}})
        return res.json(usuario)
    },

    async update(req, res){
        const usuario = await Usuario.update(req.body, {where : {id: req.params.usuario_id}});
        const find = await Usuario.findOne({where : {id: usuario}});
        return res.json(find);
    },
    async destroy(req, res){
        await Usuario.destroy({where: {id: req.params.usuario_id}});
        return res.send();
    }
}