require("dotenv-safe").config({path: __dirname + '/.env'});
    module.exports = {
        dialect: "mysql",
        host: '127.0.0.1',
        username:  process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        define: {
            timestamps: true,
            underscored: true
        }
    }
